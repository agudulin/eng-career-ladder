---
path: "/engineering/management"
title: "Engineering Management"
sidebarTitle: "Engineering Management"
sidebarGroup: "engineering"
yaml: true
levels: 5
levels_values: ["EM", "Senior EM"]
homepage: true
topics:
  - name: "Individual excellence"
    title: "Individual excellence"
    content:
      - level: 'EM'
        criteria:
          - "WIP"
      - level: 'Senior EM'
        criteria:
          - "WIP"
  - name: "Impact on the team"
    title: "Impact on the team"
    content:
      - level: 'EM'
        criteria:
          - "WIP"
      - level: 'Senior EM'
        criteria:
          - "WIP"
  - name: "Reach beyond the team"
    title: "Reach beyond the team"
    content:
      - level: 'EM'
        criteria:
          - "WIP"
      - level: 'Senior EM'
        criteria:
          - "WIP"
---
### About our engineering career ladder
The engineering career ladder is a tool that helps engineers and managers:
- make development and career plans
- talk about what we’re looking for from engineers in a consistent way
- set a fair level of compensation.

The framework is a compass, not a GPS.

It's meant to be helpful. It's not meant to be a rating system for humans, free from edge cases.

### How does it work?
The framework covers all the things we’re looking for from engineers at Babbel. We’re interested in these 3 dimensions:
- Individual excellence
- Impact on the team
- Reach beyond the team

We sort them into five levels:
- EM
- Senior EM

Your manager will work with you on this. None of it will happen mysteriously behind closed doors. You’ll agree what level of progression you’re going for and what you need to improve on with your manager. It should be clear how you’re doing relative to that at all times.

### Things to keep in mind
- There are many different ways to progress and be valuable to Babbel as you grow, including deep technical knowledge and ability, technical leadership and people management. All are equally valuable paths in every engineering team.
- The framework represents a career’s worth of progression, people shouldn’t expect to fly up it in 18 months!
- Engineering progression isn’t an exact science and there will always be some ambiguity.
- This isn’t a checklist – it’s possible to progress up a level without showing all the behaviours in that level.
- There will be levels on top (eg ‘Inventor of Android’ or ‘Author of Go’), but we won’t add them until we need them.
- You can find some more information in these links. If that doesn't answer most of your questions, please ask your manager.

### Give us your feedback!
This is only the first version of our framework and we really want your feedback.

We're particularly keen to add as many examples to the behaviours as possible, to further clarify them.

