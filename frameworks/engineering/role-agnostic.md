---
path: "/engineering/role-agnostic"
title: "Engineering Career Ladder"
sidebarTitle: "Role Agnostic"
sidebarGroup: "engineering"
yaml: true
levels: 5
levels_values: ["Working Student", "Junior","Professional","Senior","Principal"]
homepage: true
topics:
  - name: "Individual excellence"
    title: "Individual excellence"
    content:
      - level: 'Working Student'
        criteria:
          - "Develops their productivity skills by learning relevant tools and coding best practices"
          - "Can complete well-defined sub-tasks they are being assigned to and takes responsibility for them"
          - "Is receptive to constructive feedback"
      - level: 'Junior'
        criteria:
          - "Pro-actively pulls well-defined sub-tasks and drives them to completion"
          - "Drives work tasks to completion"
          - "Develops knowledge of a single component of our architecture"
          - "Effectively incorporates feedback from other members of the team"
      - level: 'Professional'
        exampleCriteria:
          - criteria: "Continuous Learning & Development"
            examples:
              - "Learns quickly and makes steady progress autonomously"
              - "Proactively solicits feedback from others and is eager to find ways to improve"
              - "Works with their manager to actively grow their skills, reading, partnering with more senior members of the team."
        criteria:
          - "Is self-sufficient in at least one large area of the product with a high-level understanding of the architecture of their project and other components"
          - "Is capable of prioritizing tasks; avoids getting caught up in unimportant details and 'bikeshedding'"
          - "Provides on-call support for their area"
          - "Can explain complex problems and solutions to people inside their domain of expertise"
          - "Learns more about other parts of the company and areas of the business"
      - level: 'Senior'
        criteria:
          - Proactively keeps themselves up-to-date with latest advances in their field of expertise
          - Autonomously engages in expanding their scope beyond the narrow confines of their roles
          - Is a go-to expert in one area of the codebase; understands the broad architecture of the entire system
          - Understands and makes well-reasoned design decisions and tradeoffs in their area; able to work in other areas of the codebase with guidance.
          - Makes sure that the system architecture of the proposed solution meets the requirements, it's scalable and maintainable, and properly documented
          - Seeks empirical evidence through proof of concepts, tests and external research
          - Reasons in terms of business impact (metrics) and understands the tradeoffs between technical, analytical and product needs and leads to solutions that take all of these needs into account
          - Communicates effectively cross functions complex problems and solutions; is able to work well with Product, Design, Analytics, etc, as necessary
      - level: 'Principal'
        criteria:
          - Investigates cutting-edge new technologies or ways of working, and reports back their team/chapter/CoP
          - Consistently able to reduce the complexity of projects, services, and processes in order to get more done with less work
          - Proactively drives the improvements in stability, performance, and scalability across major business-critical systems
          - Co-Owner of and expert on large sections of our codebase
          - Follows a data-driven based approach when making decisions.
          - Further improves communication skills. They are able to engage heterogeneous mid-sized audiences with their messages.
          - Drives the medium-long term technical strategy for their area of expertise
  - name: "Impact on the team"
    title: "Impact on the team"
    content:
      - level: 'Working Student'
        criteria:
          - "Shows interest in team/company goals and values"
          - "Collaborates with other team members and seeks mentoring support from more experienced team members"
      - level: 'Junior'
        criteria:
          - "Understands sprint priorities and goals of the team"
          - "Speaks up when others break working agreements"
          - "Positive and constructive in team interactions"
          - "Helps the team, does what needs doing"
      - level: 'Professional'
        criteria:
          - "Identifies team weaknesses and suggests solutions"
          - "Fosters team collaboration and knowledge sharing"
          - "Gives timely and helpful feedback to peers and managers"
          - "Makes other members of their team better through code reviews, thorough documentation, technical guidance, and mentoring"
      - level: 'Senior'
        criteria:
          - Further improves communication skills. Is able to clearly articulate in writing and speech messages inside of his team and with immediate stakeholders.
          - Mentors, coaches, and provides feedback to less-experienced members of the team
          - Identifies, makes it transparent to the team and proactively tackles risks / challenges / technical debt before it grows into problems that requires significant up-front work to resolve
          - Is thoughtful about process; proposes changes as needed for the whole team to execute more effectively
          - Continuously introduces best practices and ways of working
          - Splits complex deliverables and projects into manageable pieces and milestones
          - Can take a leadership role if necessary
          - Takes an active role in the fulfillment of the established SLA. Escalates when necessary.
      - level: 'Principal'
        criteria:
          - Supports other people in the team and company in their domain of expertise.
          - Shapes broad architecture; drives delivery, Has made an obvious positive impact on the entire company's technical trajectory
          - Proactively explains how new techs may open new business opportunities
  - name: "Reach beyond the team"
    title: "Reach beyond the team"
    content:
      - level: 'Professional'
        criteria:
          - "Participates in one or more Communities of Practice"
          - "When finding an issue in another team, raises the issue skillfully with that team"
      - level: 'Senior'
        criteria:
          - Finds ways to spread learning across the organization
          - "Makes engineers from other teams better through code reviews, thorough documentation, technical guidance, and mentoring"
          - Proactively researches and proposes new technologies/best practices, contributing frequently to inter-team/inter-department presentations, talks and others means of information sharing.
      - level: 'Principal'
        criteria:
          - Contributes to the vision and long-term strategy in their domain of expertise
          - Helps with multiple-team and communities of practice improvement efforts
          - Take a technical leadership role on cross-team projects, and can identify, avoid and help resolve cross-team dependencies and issues  
          - Actively works to "level-up" the company in their domain
          - Shows a broad understanding of the business context of work happening across the technology organization (beyond Engineering)
---
### About our engineering career ladder
The engineering career ladder is a tool that helps engineers and managers:
- make development and career plans
- talk about what we’re looking for from engineers in a consistent way
- set a fair level of compensation.

The framework is a compass, not a GPS.

It's meant to be helpful. It's not meant to be a rating system for humans, free from edge cases.

### How does it work?
The framework covers all the things we’re looking for from engineers at Babbel. We’re interested in these 3 dimensions:
- Individual excellence
- Impact on the team
- Reach beyond the team

We sort them into five levels:
- Working Student
- Junior Engineer
- Professional Engineer
- Senior Engineer
- Principal Engineer

Your manager will work with you on this. None of it will happen mysteriously behind closed doors. You’ll agree what level of progression you’re going for and what you need to improve on with your manager. It should be clear how you’re doing relative to that at all times.

### Things to keep in mind
- There are many different ways to progress and be valuable to Babbel as you grow, including deep technical knowledge and ability, technical leadership and people management. All are equally valuable paths in every engineering team.
- The framework represents a career’s worth of progression, people shouldn’t expect to fly up it in 18 months!
- Engineering progression isn’t an exact science and there will always be some ambiguity.
- This isn’t a checklist – it’s possible to progress up a level without showing all the behaviours in that level.
- There will be levels on top (eg ‘Inventor of Android’ or ‘Author of Go’), but we won’t add them until we need them.
- You can find some more information in the links on top. If that doesn't answer most of your questions, please ask your manager.

### Give us your feedback!
This is only the first version of our framework and we really want your feedback.

We're particularly keen to add as many examples to the behaviours as possible, to further clarify them.

